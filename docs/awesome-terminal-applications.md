# Awesome Terminal Applications <sup>[1](#footnote_1)</sup>
Sharpen the axe before you cut the wood.

![](./images/1_LuXKp-LaVj60yTNLjNRlcg.webp)

생산적인 개발자가 되기 위해서는 도구에 익숙해지는 것이 필수적이다. 때때로, 이상한 방법으로도 유연성을 발휘할 수 있다.

Linux 머신에서 사용할 수 있는 몇 가지 개발자 도구에 대해 알아보자. 이 도구들은 유용하면서도 아름답다.

시작!

> **Note**<br>
> 
> Linux에서 사용하는 도구들을 Mac에 설치하려면 먼저 [MacPort](https://www.macports.org/)를 설치하여야 한다. 

## thefuck
실수로 타이핑을 자주 하는 분들에게 유용한 도구이다. 키 입력이 100% 정확하지 않은 경우 이를 수정하는 데 도움이 된다.

설치하기

> **Note**<br>
> 
> [Mac에 thefuck 설치](https://ports.macports.org/port/thefuck/)하려면 `sudo port install thefuck`으로 설치한다.

```bash
$ brew install thefuck
```

그런 다음 명령을 잘못 입력했다는 사실을 알게 되면 `fuck`을 입력하기만 하면 자동으로 수정되어 실행된다.

![](./images/1_AcOh0vDcHX-K60tx60NBgQ.webp)

일부러 `pithin3`을 입력했다가 존재하지 않는다는 것을 볼 수 있다.

그런 다음 `fuck`를 입력하자 올바른 옵션이 표시되었다.

`enter`를 눌러 명령을 실행할지, 아니면 `cntrl+c`를 눌러 명령을 취소할지 선택할 수 있다.

간단하고 재미있나요!

## btop
시스템 성능을 모니터링하고 무슨 일이 일어나고 있는지 이해하기 위해 `top`이라는 기본 도구가 있다.

터미널로 이동하여

```bash
$ top
```

아래와 같이 출력된다.

![](./images/1_t2PZKPENEcVYcT0YSK6n0Q.webp)

여기에는 기본 시스템 상태, 프로세스 수 등이 표시된다.

하지만 이 도구의 더 나은 버전이 있다.

먼저 설치해 보자.

```bash
$ brew install btop

# after the command is successful run

$ btop
```

그러면 다음과 같은 출력이 표시된다.

얼마나 멋진가?

![](./images/1_RYQpOSzkFM58XMEtCK8gEQ.webp)

하지만 잠깐만, 이것은 단지 아름다운 것만이 아니다. 기능적인 측면도 있다.

바로 가기가 제공되는 것을 볼 수 있다. 마우스나 키보드를 사용하여 불필요한 프로세스를 탐색하고 종료할 수 있다.

## fzf
폴더에 있는 모든 파일을 찾는 데 유용한 도구이다.

특정 폴더의 모든 파일과 디렉토리를 검색한다.

먼저 설치해 보자.

```bash
$ brew install fzf
```

그런 다음 원하는 폴더로 이동한다. 예를 들어 문서 폴더로 이동한다.

```bash
$ cd documentation
```

`fuzzy finder`를 사용하려면, 

```bash
$ fzf
```

이제 원하는 파일 이름을 입력하면 매우 빠르게 파일을 찾을 수 있다.

![](./images/1_ZloV-gSDd0DmAUUVKWFe2g.webp)

`app.tsx`가 있는 파일과 경로를 검색하고 있다.

여기에는 검색하고자 하는 것과 일치하는 모든 파일 또는 경로가 나열된다.

꽤 강력하다.

## tldr
터미널에서 다른 도구를 사용하는 방법을 알려주는 또 다른 파일이다.

터미널에서 특정 도구를 사용하는 방법이 기억나지 않거나 명령을 잊어버렸더라도 걱정하지 마세요!

설치하자.

```bash
$ brew install tldr
```

터미널에서 `grep` 명령을 사용하려고 하는데 명령을 사용하는 방법을 잊어버렸다고 가정해 보겠다.

터미널로 가서 입력할 수 있다.

```bash
$ tldr grep
```

이렇게 하면 군더더기 없이 빠르고 기능적인 문서가 표시된다.

![](./images/1_Xx20IxTVlCMHEg6rU8t17A.webp)

일상에 매우 편리하다.


<a name="footnote_1">1</a>: 이 페이지는 [Awesome Terminal Applications](https://levelup.gitconnected.com/awesome-terminal-applications-e4a06022dffa)을 편역한 것임.
